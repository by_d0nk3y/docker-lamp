#  LAMP stack built with Docker Compose

Un environnement de base LAMP construit à l'aide de Docker Compose. Il se compose des éléments suivants :

    PHP
    Apache
    MySQL
    phpMyAdmin
    Redis

Utilisez la version PHP appropriée selon vos besoins :

    - 7.4.x
    - 8.1.x
    
    
## Info

Cette application web est toujours en cours de développement et me sert d'environnement pour l'étude des failles et d'injection sql.

## Installation

Clonez ce dépôt sur votre ordinateur : 

```shell
git clone https://github.com...
cd container-lamp/
```

Puis configurez les fichiers sample.env et config.php (information bdd) selon vos besoins. Le projet est configurer avec les information suivante :

- Fichier src/config/config.php :
```php
define("DBHOST", 'mysql:host=database:3306'); 
define("DBNAME", 'bibliotheque'); 
define("DBUSER", 'root'); 
define("DBPASS", 'letmepass'); 
```

- Fichier sample.env :
```env
# Mot de passe de l'utilisateur root MySQL
MYSQL_ROOT_PASSWORD=letmepass
```

Controler les informations restantes, une fois fait exécutez les commandes suivante :

```shell
cp sample.env .env
docker-compose up -d
```

## phpMyAdmin

phpMyAdmin est configuré pour fonctionner sur le port 8080. Utilisez les identifiants par défaut suivants.

```shell
http://localhost:8080/
nom d'utilisateur : root
mot de passe : letmepass
```

Le projet n'est pas configurer pour créer la base de données `bibliotheque` automatiquement. Il est donc primordiale de la créer manuellement.
Pour cela un fichier est fournit dans `src/Persistance/bdd.sql`

-- Création de la base de données
`CREATE DATABASE IF NOT EXISTS bibliotheque;`

La gestion des utilisateurs création et login est gérer en php cepandant je vous conseille d'executer l'insertion des books `src/Persistance/bdd.sql` en bdd pour un fonctionnement optimal de l'application.

-- Exemple d'ajout d'un livre
```
INSERT INTO livres (book_title, book_author, book_synopsis, book_release_date, book_category, book_available) VALUES
("Dragon ball", "Akira Toryiama", "synopsis", "1987-10-07", "manga", true),
("L'heresie d'horus", "Dan Abnett", "synopsis", "2000-02-08", "roman", true),
("Civil war", "Mark Millar", "synopsis", "2005-04-06", "comics", true);
```

## Redis

Il fonctionne sur le port par défaut `6379`.



