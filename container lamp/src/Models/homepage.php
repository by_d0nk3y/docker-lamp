<?php

require_once 'Persistance/database.php';	

class Homepage {

    private $bdd;

    public function __construct() {
    
        // Initialisation du modèle de base de données dans le constructeur du contrôleur
        $this->bdd = Database::getInstance();

    }
    

    public function getBooks() {

    	try {
            // Utilisez $this->databaseModel pour effectuer des opérations sur la base de données
            $stmt = $this->bdd->query('SELECT * FROM books');
            $result = $stmt->fetchAll();

            return $result;
            
        } catch (\PDOException $e) {
            // Gérez les erreurs de base de données
            echo "Erreur de base de données : " . $e->getMessage();
        }
    }
}


