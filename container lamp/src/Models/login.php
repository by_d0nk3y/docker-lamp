<?php

require_once 'Persistance/database.php';	

class Login {

    private $bdd;

    public function __construct() {
    
        // Initialisation du modèle de base de données dans le constructeur du contrôleur
        $this->bdd = Database::getInstance();

    }
    	
    public function getUser($email) {

    	try {

	    $userBdd = $this->bdd->prepare('SELECT * FROM users WHERE user_email = ?');
	    $userBdd->execute([$email]);
	    $user = $userBdd->fetch(PDO::FETCH_ASSOC);
	    
	    // Récupérer tous les rôles de l'utilisateur s'il en a plusieurs
	    $userRoles = $this->bdd->prepare('
		    SELECT roles.role_name
		    FROM roles
		    JOIN users_roles ON roles.role_id = users_roles.role_id
		    WHERE users_roles.user_id = ?
	    ');
	    $userRoles->execute([$user['user_id']]);
	    $roles = $userRoles->fetchAll(PDO::FETCH_COLUMN);
	    
	    // Ajouter la liste des rôles à la variable $user
	    $user['role_name'] = $roles;

            return $user;
            
        } catch (\PDOException $e) {
            // Gérez les erreurs de base de données
            echo "Erreur de base de données : " . $e->getMessage();
        }
    }
}






