<?php

require_once 'Persistance/database.php';	

class Register {

    private $bdd;

    public function __construct() {
    
        // Initialisation du modèle de base de données dans le constructeur du contrôleur
        $this->bdd = Database::getInstance();

    }
    

    public function dbUser($user_lastname, $user_firstname, $user_email, $user_password) {
    
    	$this->bdd->beginTransaction(); 

    	try {
    	
            $userRoles = ["utilisateur"];
    	
    	    // Étape 1 : Enregistrement de l'utilisateur dans la table users
	    $addUser = $this->bdd->prepare('INSERT INTO users (user_lastname, user_firstname, user_email, user_password, user_registration_date) VALUES (?, ?, ?, ?, CURDATE())');
	    $addUser->execute([$user_lastname, $user_firstname, $user_email, $user_password]);
	    
	    // Étape 2 : Récupération de l'ID de l'utilisateur nouvellement inséré
            $userId = $this->bdd->lastInsertId();
            
            // Étape 3 : Ajout des rôles dans la table roles et attribution des rôles dans la table users_roles
            foreach($userRoles as $userRole) {
            
                // Vérification si le rôle existe déjà
        	$checkRole = $this->bdd->prepare('SELECT role_id FROM roles WHERE role_name = ?');
        	$checkRole->execute([$userRole]);
        	$roleId = $checkRole->fetchColumn();
            
		if (!$roleId) {
		    // Le rôle n'existe pas, on l'ajoute
		    $addRole = $this->bdd->prepare('INSERT INTO roles (role_name) VALUES (?)');
		    $addRole->execute([$userRole]);

		    // Récupération de l'ID du rôle nouvellement inséré
		    $roleId = $this->bdd->lastInsertId();
		}

		// Attribution du rôle à l'utilisateur dans la table users_roles
		$addUserRole = $this->bdd->prepare('INSERT INTO users_roles (user_id, role_id) VALUES (?, ?)');
		$addUserRole->execute([$userId, $roleId]);
            
            }
            
            // Toutes les étapes ont réussi, on valide
    	    $this->bdd->commit();


            
        } catch (\PDOException $e) {
            // Gérez les erreurs de base de données
            $this->bdd->rollBack();
            echo "Erreur de base de données : " . $e->getMessage();
        }
    }
}





