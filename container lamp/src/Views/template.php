<!DOCTYPE html>
<html lang="fr">
    <head>
    	<?php include_once 'Views/includes/head.php' ?>
        <title> <?php echo ucfirst(strtolower($page)); ?> </title>
    </head>
    
    <body>
    
    <header class="hero is-medium is-info is-bold">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">
                    LAMP STACK
                </h1>
                <h2 class="subtitle">
                    Environnement de développement local
                </h2>
            </div>
        </div>
    </header>
        
	<nav class="navbar mb-4" role="navigation" aria-label="main navigation">
	    <div class="navbar-brand">
            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            </a>
	    </div>

	    <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item">
                    Accueil
                </a>

                <a class="navbar-item">
                    Documentation
                </a>

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        Plus
                    </a>

                    <div class="navbar-dropdown">
                        <a class="navbar-item">
                            À propos
                        </a>
                        <a class="navbar-item">
                            Travaux
                        </a>
                        <a class="navbar-item">
                            Contact
                        </a>
                        <hr class="navbar-divider">
                        <a class="navbar-item">
                            Signaler un problème
                        </a>
                    </div>
                </div>
		    </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="buttons">
                        <a class="button is-primary" href="/register">
                            <strong>Créer un compte</strong>
                        </a>
                        
                        <?php 
                        if (isset($_SESSION['user_id'])) { ?>

		                <a class="button is-light" href="/logout" type="submit">
		                    Deconnexion
		                </a>

			<?php } else { ?>
		                <a class="button is-light" href="/login" type="submit">
		                    Login
		                </a>
			<?php } ?>
    
                    </div>
                </div>
            </div>
	    </div>
	</nav>
        
    <main class="container is-fluid">
    

	<?php if (isset($_SESSION['user_id'])) { ?>
	    <section class="columns is-multiline"> 
	        <div class="column">	
		    <p>Utilisateur connecté : <?php echo $_SESSION['user_email']; ?></p>
	        </div>
	    </section> 
	    
	<?php } ?>
	
        <!-- Contenu spécifique à chaque page sera inséré ici -->
        <?php include 'Views/includes/' . $content; ?>	
    
    </main>
        
    <footer>
        <!-- Pied de page commun -->
    </footer>
    	
    	
    <script>
        document.addEventListener('DOMContentLoaded', () => {

            // Get all "navbar-burger" elements
            const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Add a click event on each of them
            $navbarBurgers.forEach(el => {
                el.addEventListener('click', () => {

                    // Get the target from the "data-target" attribute
                    const target = el.dataset.target;
                    const $target = document.getElementById(target);

                    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                    el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');

                });
            });
        });
    </script>
    
    </body>
</html>