<?php

// Démarrez la session
session_start([
    'cookie_lifetime' => 60, // Durée de vie du cookie de session en secondes
    'cookie_secure' => true, // Utiliser uniquement sur des connexions HTTPS
    'cookie_httponly' => true, // Rend le cookie inaccessible via JavaScript
    'use_strict_mode' => true, // Applique un mode strict pour les sessions
]);

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// Inclusion des fichiers de configuration et de la gestion de la base de données
require_once 'Config/config.php';

// Définition de la page courante
if (isset($_GET['page']) AND !empty($_GET['page'])) {
    $page = trim(strtolower($_GET['page']));

} else {
    $page = 'homepage';
}

// Array contenant toutes les pages
$allPages = scandir('Controllers/');

// Vérification de l'existence de la page
if (in_array($page.'.php', $allPages)) {
    include_once 'Controllers/'.$page.'.php';

} else {
    // Redirection vers une page d'erreur 404 si le fichier du contrôleur n'est pas trouvé
    header("HTTP/1.0 404 Not Found");
    echo 'Page non trouvée';

exit();
}
    
    
function includeView($view, $data = []) {	

    // La fonction accepte deux paramètres :
    // $view : Le nom du fichier de vue à inclure (par exemple, 'homepage.php').
    // $data : Un tableau associatif contenant des données à passer au template (par défaut, un tableau vide).
    
    extract($data);
    // La fonction extract() importe les variables d'un tableau associatif dans la table des symboles.
    // En d'autres termes, elle crée des variables portant les noms des clés du tableau et leur assigne les valeurs correspondantes.
    // Cela permet d'accéder directement aux données dans le template sans utiliser $data['nom_variable'].
    
    ob_start();
    // ob_start() active la temporisation de sortie (output buffering) en PHP.
    // Cela signifie que la sortie générée à partir de maintenant sera stockée en mémoire tampon au lieu d'être envoyée directement au navigateur
    
    include 'Views/' . $view;
    // On inclut le fichier de vue dans la mémoire tampon.
    // Le chemin 'views/' . $view est utilisé pour spécifier le répertoire où se trouvent les fichiers de vue.
    
    return ob_end_flush();
    // ob_get_clean() récupère le contenu actuel de la mémoire tampon et l'efface.
    // Cela nous permet de récupérer le contenu du fichier de vue inclus et de le renvoyer.

}
    