<?php

require_once 'Models/homepage.php';


if (isset($_SESSION['user_id'])) {

    $db = new Homepage();
    $databooks = $db->getBooks();
    
    $content = 'books.php';
    includeView('template.php', ['content' => $content, 'databooks' => $databooks]);

} else {

    // Redirection vers la page de connexion si l'utilisateur n'est pas connecté
    header('Location: login');
    exit();
}