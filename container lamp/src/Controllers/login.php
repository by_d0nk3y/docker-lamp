<?php


require_once 'Models/login.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	// Récupérer les données du formulaire
	$email = isset($_POST['email']) ? $_POST['email'] : '';
	$password = isset($_POST['password']) ? $_POST['password'] : '';

	// Valider les données
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
	    
	    // Rechercher l'utilisateur dans la base de données
	    $login = new Login();
	    $user = $login->getUser($email);

	    if ($user && password_verify($password, $user['user_password'])) {
	    
		// Mot de passe correct, l'utilisateur est authentifié
		// echo "Identifiants correct !";
		
		$_SESSION['user_id'] = $user['user_id'];
		$_SESSION['user_email'] = $user['user_email'];
		$_SESSION['user_lastname'] = $user['user_lastname'];
		$_SESSION['user_firstname'] = $user['user_firstname'];
		$_SESSION['user_roles'] = $user['role_name']; 

		// Redirection vers la page d'accueil ou autre page sécurisée
		header('Location: homepage');
		exit();

	    } else {
        	// Mot de passe incorrect
        	echo "Identifiants incorrects!";
	    }
	} else {
	    // L'adresse email n'est pas valide
	    echo "Adresse email non valide!";
	}
}


// Inclure la vue de connexion
$content = 'login.php';
includeView('template.php', ['content' => $content]);
