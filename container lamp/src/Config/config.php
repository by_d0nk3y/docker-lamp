<?php

// --------------------------- //
//         CONSTANTS           //
// --------------------------- //

// Paths
define("PATH_REQUIRE", substr($_SERVER['SCRIPT_FILENAME'], 0, -9)); // Pour fonctions d'inclusion php
define("PATH", substr($_SERVER['PHP_SELF'], 0, -9)); // Pour images, fichiers etc (html)

// Website informations
define("WEBSITE_TITLE", "Développeur Back-End");
define("WEBSITE_NAME", "Alexandre MAURY");
define("WEBSITE_URL", "https://alexandre-maury.com");
define("WEBSITE_DESCRIPTION", "Freelance : Développement d'application / Création de sites Internet / Pentesting");
define("WEBSITE_KEYWORDS", "PHP, Python, Docker, API-REST, Pentest");
define("WEBSITE_LANGUAGE", "fr");
define("WEBSITE_AUTHOR", "Alexandre MAURY");
define("WEBSITE_AUTHOR_MAIL", "alexandre.maury@outlook.com");

// Facebook Open Graph tags
define("WEBSITE_FACEBOOK_NAME", "");
define("WEBSITE_FACEBOOK_DESCRIPTION", "");
define("WEBSITE_FACEBOOK_URL", "");
define("WEBSITE_FACEBOOK_IMAGE", "");

// DataBase informations
define("DBHOST", 'mysql:host=database:3306'); 
define("DBNAME", 'bibliotheque'); 
define("DBUSER", 'root'); 
define("DBPASS", 'letmepass'); 

// Language
define("DEFAULT_LANGUAGE", "fr");

// Adresse Email
define("EMAIL_ADMIN", "alexandre.maury@outlook.com");